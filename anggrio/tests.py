from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import profilePage
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class Story8UnitTests(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_profile_page_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profilePage)

    def test_profile_page_using_profile_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'profile-page.html')

    def test_profile_page_is_completed(self):
        request = HttpRequest()
        response = profilePage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Profile', html_response)

class Story8FunctionalTests(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_can_toggle_accordion_section_hidden_and_shown(self):
        self.browser.get('http://localhost:8000')
        self.assertIn("Anggrio Wildanhadi Sutopo's Profile", self.browser.title)
        time.sleep(5)

        panel = self.browser.find_element_by_id('panel1')
        flip = self.browser.find_element_by_id('flip1')
        self.assertEqual("none", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel1')
        self.assertEqual("block", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel1')
        self.assertEqual("none", panel.value_of_css_property("display"))
        time.sleep(5)

        panel = self.browser.find_element_by_id('panel2')
        flip = self.browser.find_element_by_id('flip2')
        self.assertEqual("none", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel2')
        self.assertEqual("block", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel2')
        self.assertEqual("none", panel.value_of_css_property("display"))
        time.sleep(5)

        panel = self.browser.find_element_by_id('panel3')
        flip = self.browser.find_element_by_id('flip3')
        self.assertEqual("none", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel3')
        self.assertEqual("block", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel3')
        self.assertEqual("none", panel.value_of_css_property("display"))
        time.sleep(5)

        panel = self.browser.find_element_by_id('panel4')
        flip = self.browser.find_element_by_id('flip4')
        self.assertEqual("none", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel4')
        self.assertEqual("block", panel.value_of_css_property("display"))
        flip.click()
        time.sleep(5)
        panel = self.browser.find_element_by_id('panel4')
        self.assertEqual("none", panel.value_of_css_property("display"))

    def test_can_move_each_accordion_section_up_and_down(self):
        self.browser.get('http://localhost:8000')
        time.sleep(5)

        panel_title = self.browser.find_element_by_class_name("border").text
        self.assertIn("About Me", panel_title)
        flip = self.browser.find_element_by_id('flip1')
        flip.click()
        time.sleep(5)
        body = self.browser.find_element_by_tag_name("p").text
        button = self.browser.find_element_by_id("down1")
        self.assertIn("Anggrio", body)
        button.click()
        time.sleep(5)

        panel_title = self.browser.find_element_by_class_name("border").text
        self.assertIn("Current Activities", panel_title)
        flip = self.browser.find_element_by_id('flip2')
        flip.click()
        time.sleep(5)
        body = self.browser.find_element_by_tag_name("p").text
        self.assertIn("Introduction", body)
        button = self.browser.find_element_by_id("up1")
        button.click()
        time.sleep(5)

        panel_title = self.browser.find_element_by_class_name("border").text
        self.assertIn("About Me", panel_title)
        body = self.browser.find_element_by_tag_name("p").text
        self.assertIn("Anggrio", body)

    def test_can_change_theme_after_button_press(self):
        self.browser.get('http://localhost:8000')
        time.sleep(5)

        background = self.browser.find_element_by_tag_name("body")
        self.assertEqual("rgba(1, 238, 255, 1)", background.value_of_css_property("background-color"))
        button = self.browser.find_element_by_id("change")
        button.click()
        time.sleep(5)

        background = self.browser.find_element_by_tag_name("body")
        self.assertEqual("rgba(255, 238, 1, 1)", background.value_of_css_property("background-color"))
        button.click()
        time.sleep(5)

        background = self.browser.find_element_by_tag_name("body")
        self.assertEqual("rgba(1, 238, 255, 1)", background.value_of_css_property("background-color"))
        
