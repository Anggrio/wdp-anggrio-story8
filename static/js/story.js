$(document).ready(function(){
    $("#flip1").click(function(){
        $("#panel1").slideToggle("slow");
    });
    $("#flip2").click(function(){
        $("#panel2").slideToggle("slow");
    });
    $("#flip3").click(function(){
        $("#panel3").slideToggle("slow");
    });
    $("#flip4").click(function(){
        $("#panel4").slideToggle("slow");
    });
    
    $("#down1").click(function(){
        $("#box1").insertAfter($("#box1").next());
    });

    $("#down2").click(function(){
        $("#box2").insertAfter($("#box2").next());
    });

    $("#down3").click(function(){
        $("#box3").insertAfter($("#box3").next());
    });

    $("#down4").click(function(){
        $("#box4").insertAfter($("#box4").next());
    });

    $("#up1").click(function(){
        $("#box1").insertBefore($("#box1").prev());
    });

    $("#up2").click(function(){
        $("#box2").insertBefore($("#box2").prev());
    });

    $("#up3").click(function(){
        $("#box3").insertBefore($("#box3").prev());
    });

    $("#up4").click(function(){
        $("#box4").insertBefore($("#box4").prev());
    });

    $("#change").click(function(){
        if ($("html").css("background-color")=="rgb(1, 238, 255)"){
            $("html").css("background-color", "rgba(255, 238, 1, 1)");
            $("body").css("background-color", "rgba(255, 238, 1, 1)");
            $("div strong").css("color", "#ff0000");
            $("h1 strong").css("color", "#ff0000");
            $(".p-2.d-flex").css("background-color", "#ff4500");
            $(".border").css("css-text", "border-color: #ff0000!important");
            $(".btn-primary").css({
                "background-color": "#ff4500", 
                "border-color": "#ff0000!important",
                "color": "#ff0000"
            });
            $(".p-2.border").css({
                "color": "#ff0000",
                "background-color": "#ffa500!important"
            })
            $(".bg-light").css({
                "background-color": "#ffa500!important",
                "border-color": "#ff0000!important"
            });
        } else {
            $("html").css("background-color", "rgba(1, 238, 255, 1)");
            $("body").css("background-color", "rgba(1, 238, 255, 1)");
            $("div strong").css("color", "rgba(255, 255, 255, 0.692)");
            $("h1 strong").css("color", "rgba(0, 0, 0, 0.527)");
            $(".p-2.d-flex").css("background-color", "#007bff");
            $(".border").css("css-text", "border-color: #343a40!important");
            $(".btn-primary").css({
                "background-color": "#007bff", 
                "border-color": "#ff0000!important",
                "color": "white"
            });
            $(".p-2.border").css({
                "color": "#212529",
                "background-color": "#f8f9fa!important"
            })
        }
        
    })
});